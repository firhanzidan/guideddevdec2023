﻿namespace Terrasoft.Configuration
{

	using System;
	using System.Collections.Generic;
	using System.Collections.ObjectModel;
	using System.Globalization;
	using Terrasoft.Common;
	using Terrasoft.Core;
	using Terrasoft.Core.Configuration;

	#region Class: UsrRealtyEventsSchema

	/// <exclude/>
	public class UsrRealtyEventsSchema : Terrasoft.Core.SourceCodeSchema
	{

		#region Constructors: Public

		public UsrRealtyEventsSchema(SourceCodeSchemaManager sourceCodeSchemaManager)
			: base(sourceCodeSchemaManager) {
		}

		public UsrRealtyEventsSchema(UsrRealtyEventsSchema source)
			: base( source) {
		}

		#endregion

		#region Methods: Protected

		protected override void InitializeProperties() {
			base.InitializeProperties();
			UId = new Guid("d7520284-8568-4741-9310-29c8f857b5a1");
			Name = "UsrRealtyEvents";
			ParentSchemaUId = new Guid("50e3acc0-26fc-4237-a095-849a1d534bd3");
			CreatedInPackageId = new Guid("f096c9c2-7b71-4b01-a006-297530091a17");
			ZipBody = new byte[] { 31,139,8,0,0,0,0,0,4,0,141,82,77,107,219,64,16,189,235,87,12,34,7,9,204,146,92,235,54,80,27,167,24,66,82,98,185,151,210,195,122,53,86,54,236,135,152,93,57,117,139,255,123,119,181,118,98,217,41,201,156,164,217,55,111,222,123,140,225,26,93,203,5,66,133,68,220,217,181,103,83,107,214,178,233,136,123,105,77,246,55,131,80,157,147,166,129,197,214,121,212,227,163,206,241,148,214,214,252,239,141,144,205,140,151,94,162,251,0,132,205,54,104,252,30,249,179,239,110,251,214,173,12,2,12,82,177,16,143,168,249,93,80,15,95,32,95,58,122,64,174,252,246,134,16,107,171,151,243,188,252,213,15,183,221,74,73,1,66,113,231,32,97,222,160,131,79,48,225,14,223,120,233,73,82,4,71,116,118,19,116,203,26,97,99,101,13,247,102,193,55,193,77,97,87,79,40,60,56,52,53,210,8,18,221,4,215,193,90,79,250,149,26,7,88,190,176,189,242,198,90,5,5,236,133,235,64,130,229,120,128,74,164,64,189,149,224,189,72,141,50,225,135,216,26,133,212,92,65,75,82,196,156,210,16,251,134,190,218,182,88,79,173,234,180,249,193,85,135,159,247,208,235,34,102,249,61,226,243,147,197,114,13,69,34,186,134,171,203,67,149,3,204,208,80,44,100,115,55,229,70,160,194,58,40,240,212,225,56,59,67,57,79,241,26,194,41,58,222,96,133,186,85,220,71,197,6,159,225,214,10,174,228,31,190,82,184,232,113,197,222,199,210,33,133,91,53,33,243,112,168,236,1,157,237,72,4,144,165,192,50,130,179,53,177,94,111,37,221,88,62,130,252,108,131,99,125,42,115,87,89,59,145,77,250,203,75,86,217,189,130,242,93,19,65,124,106,176,27,75,154,251,226,196,92,88,123,197,46,39,23,167,49,199,242,143,100,159,123,239,179,223,2,219,232,238,48,126,130,222,101,195,175,93,182,251,7,245,71,83,51,210,3,0,0 };
		}

		protected override void InitializeLocalizableStrings() {
			base.InitializeLocalizableStrings();
			SetLocalizableStringsDefInheritance();
			LocalizableStrings.Add(CreateValueIsTooBigLocalizableString());
		}

		protected virtual SchemaLocalizableString CreateValueIsTooBigLocalizableString() {
			SchemaLocalizableString localizableString = new SchemaLocalizableString() {
				UId = new Guid("69162c1c-6e49-a1d5-a62a-ea2a36bc2c8b"),
				Name = "ValueIsTooBig",
				CreatedInPackageId = new Guid("f096c9c2-7b71-4b01-a006-297530091a17"),
				CreatedInSchemaUId = new Guid("d7520284-8568-4741-9310-29c8f857b5a1"),
				ModifiedInSchemaUId = new Guid("d7520284-8568-4741-9310-29c8f857b5a1")
			};
			return localizableString;
		}

		#endregion

		#region Methods: Public

		public override void GetParentRealUIds(Collection<Guid> realUIds) {
			base.GetParentRealUIds(realUIds);
			realUIds.Add(new Guid("d7520284-8568-4741-9310-29c8f857b5a1"));
		}

		#endregion

	}

	#endregion

}

